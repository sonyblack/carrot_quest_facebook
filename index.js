const puppeteer = require('puppeteer');
const scrollPageToBottom = require('puppeteer-autoscroll-down');


(async() => {
  const {google} = require('googleapis')
  const keys = require('./keys.json')
  const client = new google.auth.JWT(
      keys.client_email, 
      null, 
      keys.private_key,
      ['https://www.googleapis.com/auth/spreadsheets']
  )
  // Запустим браузер
  const browser = await puppeteer.launch({
        args: [
          '--disable-notifications',
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-infobars',
          '--window-position=0,0',
          '--ignore-certifcate-errors',
          '--ignore-certifcate-errors-spki-list'
        ],
        headless: true
    }
  );
  // Откроем новую страницу
  const page = await browser.newPage();
  //const grabUrl = 'https://www.facebook.com/groups/www.planetaperm.ru/';
  const grabUrl = 'https://www.facebook.com/groups/kinoperm/';
  //const grabUrl = 'https://www.facebook.com/groups/appleperm/';
  
  try {
    await page.goto('https://mbasic.facebook.com/');

    await page.setViewport({ width: 1440, height: 800 })

    await page.waitForSelector('#m_login_email');
    await page.click('#m_login_email');
    await page.type('#m_login_email', 'nikolay.sb@gmail.com');

    await page.waitForSelector('input[type=password]');
    await page.click('input[type=password]');
    await page.type('input[type=password]', 'j7pQ9TSz');

    await page.click('input[type=submit]');

    await page.goto(grabUrl + 'members/');

    const groupName = await page.evaluate(() => {return document.querySelector('#seo_h1_tag').textContent});

    console.log('Зашли в группу ' + groupName + ', прокручиваем вниз, это может занять время.')

    const scrollStep = 400
    const scrollDelay = 1000
    const lastPosition = await scrollPageToBottom(page, scrollStep, scrollDelay)

    const urls = await page.$$eval('div[data-testid=GroupMemberGrid] .uiProfileBlockContent a', items => items.map(item => item.href.split('fref')[0]));

    let data = [];
    let i = 2
    let pause = 1

    for (let url of urls) {
      if (pause == 150) {
        pause = 1
        await page.waitFor(600000);
      }

      try {
        await page.goto(url);
        await page.waitFor(70000);
      } catch {
        await page.goto(url);
        await page.waitFor(50000);
      }
      
      let name = ''
      try {
        name = await page.$eval('#fb-timeline-cover-name', item => item.textContent);
      } catch {
        name = await page.$eval('#fb-timeline-cover-name', item => item.textContent);
      }
      
      let position = ''
      try {
        position = await page.$eval('#intro_container_id li[data-profile-intro-card="1"]', item => item.textContent);
      } catch {}
      
      let location = ''
      try {
        location = await page.$eval('a[data-hovercard*=city]', item => item.textContent);
      } catch {}
      data.push([name,url,position,location])

      console.log(i + ' - ' + name)
      
      client.authorize(function(err,tokens) {
        if (err) {
            console.log(err)
        } else {
            gsRun (client)
            i++
            pause++
        }
      })
      async function gsRun (cl) {
          const gsapi = google.sheets({ version:'v4', auth:cl })
          const opt = {
              spreadsheetId: '1i3r9uORKvCL_SNCAgkbZ1cPL4wNyx7BEBZID5RVY7QE',
              range: 'Лист1!A2',
              valueInputOption: 'USER_ENTERED',
              resource: { values: data }
          }
          let table = await gsapi.spreadsheets.values.update(opt)
      }
    }    

  } catch (error) {
    console.log(`Не удалось открыть страницу: ${grabUrl} из-за ошибки: ${error}`)
  }
  // Всё сделано, закроем браузер
  await browser.close()
  
  process.exit()
})();